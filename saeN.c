#include <stdio.h>
#include <string.h>
#include "saeN.h"
#define TAILLE 50

void chargementInit (void)
{
	int idSports[TAILLE], nbPtSpo[TAILLE], nSports, choix;/*nb de sports, choix*/;
	int tNC[50], tA[50], tCA[50], tPO[50]; //pour tes fonctions
	int  nAdherent; /*nb d'adherents*/ //J'ai enlevé Tmax vu que j'avais déjà define TAILLE à 50 , mais faudra probablement y changer vu que ça fait pas bcp 50 adhérents
	char tN[50],tPR[50],venu[50] = {0}, listeSports[50][20];
	nSports = chargementSport(idSports,nbPtSpo,TAILLE,listeSports);
	nAdherent = chargementadherent(tNC,tN,tPR,tA,tCA,tPO);
	printf("\nBienvenue , veuillez choisir le type d'action que vous voulez faire \n");
	menuChoix(choix,idSports,nbPtSpo,&nSports,tNC,tA,tCA,tPO,&nAdherent,tN,tPR,venu,listeSports);
}

int menuChoix (int choix, int *idSports, int *nbPtSpo, int *nSports, int *tNC, int *tA, int *tCA, int *tPO, int *nAdherent, char *tN, char *tPR, char *venu, char listeSports[][20])
{
	printf("------------------------------------------------\n"); //tableau qui s'affiche en premier pour choisir 
	printf("| 	Choix de la gestion  		       |\n"); 
	printf("|				    	       |\n"); 
	printf("| 1.	Gestion des activités		       |\n");
	printf("| 2.	Gestion des adhérents	   	       |\n"); 
	printf("|				    	       |\n"); //espace libre , vérifie que ça s'affiche bien en testant si tu y change (tu peux toujours rajouter plus de fonctions stv)
	printf("| 9.	Quitter			    	       |\n"); 
	printf("|				    	       |\n");
	printf("------------------------------------------------\n"); //tableau qui s'affiche en premier pour choisir 
	printf("Rentrez un chiffre : \n"); //choix de l'option
	scanf("%d%*c",&choix);
	if (choix == 1)
		{printf("\n");choixMenuAct(choix,idSports,nbPtSpo,nSports,tNC,tA,tCA,tPO,nAdherent,tN,tPR,venu,listeSports);return 0;}
	if (choix == 2)
		{printf("\n");choixMenuAd(choix,idSports,nbPtSpo,nSports,tNC,tA,tCA,tPO,nAdherent,tN,tPR,venu,listeSports);return 0;}
	if (choix == 9) //quitte le logiciel si l'utilisateur a rentré 9
		{printf("\n"); printf("Arrêt du logiciel");printf("\n"); printf("\n");}
	if (choix != 9)
		menuChoix (choix,idSports,nbPtSpo,nSports,tNC,tA,tCA,tPO,nAdherent,tN,tPR,venu,listeSports);
}

int choixMenuAct (int choix, int *idSports, int *nbPtSpo, int *nSports, int *tNC, int *tA, int *tCA, int *tPO, int *nAdherent, char *tN, char *tPR, char *venu, char listeSports[][20])
{
	printf("------------------------------------------------\n"); //tableau qui s'affiche en premier pour choisir 
	printf("| 	Gestion des activités   	       |\n"); 
	printf("|				    	       |\n"); 
	printf("| 1.	Affichage des Activités		       |\n");
	printf("| 2.	Ajout d'une Activité	   	       |\n"); 
	printf("| 3.	Suppression d'une Activité  	       |\n"); 
	printf("| 4.	Changer le prix d'une activité	       |\n");
	printf("|				    	       |\n");
	printf("| 9.	Quitter			    	       |\n"); 
	printf("|				    	       |\n");
	printf("------------------------------------------------\n"); //tableau qui s'affiche en premier pour choisir 
	printf("Rentrez un chiffre : \n"); //choix de l'option
	scanf("%d%*c",&choix);
	if (choix == 1)
		{printf("\n");affichage(idSports,nbPtSpo,*nSports,listeSports); printf("\n");}
	if (choix == 2)
		{printf("\n");ajoutSport(idSports,nbPtSpo,nSports,TAILLE,listeSports); printf("\n");}
	if (choix == 3)
		{printf("\n");enlèveSport(idSports,nbPtSpo,nSports,listeSports);printf("\n");}
	if (choix == 4)
		{printf("\n");changprix(idSports,nbPtSpo,*nSports);printf("\n");}
	if (choix == 9) //quitte le logiciel si l'utilisateur a rentré 9
		{printf("\n"); printf("Arrêt du logiciel"); printf("\n"); printf("\n");}
	if (choix != 9)
		menuChoix (choix,idSports,nbPtSpo,nSports,tNC,tA,tCA,tPO,nAdherent,tN,tPR,venu,listeSports);
	sauvegardeSports(idSports,nbPtSpo,*nSports,listeSports);
	return 0;
}

int choixMenuAd (int choix, int *idSports, int *nbPtSpo, int *nSports, int *tNC, int *tA, int *tCA, int *tPO, int *nAdherent, char *tN, char *tPR, char *venu, char listeSports[][20])
{
	printf("-------------------------------------------------\n"); //tableau qui s'affiche en premier pour choisir 
	printf("| 	Gestion des adhérents    	        |\n"); 
	printf("|				    	        |\n"); 
	printf("| 1.	Afficher les adhérents		        |\n");
	printf("| 2.	Ajouter un adhérent	       	        |\n");
	printf("| 3.	Arrivée d'un client	       	        |\n"); 
	printf("| 4.	Ajout de points			        |\n");
	printf("| 5.	Suppression d'un adhérent 	   	|\n"); 
	printf("|				    	        |\n"); 
	printf("| 9.	Quitter			    	        |\n"); 
	printf("|				    	        |\n");
	printf("--------------------------------------------------\n"); //tableau qui s'affiche en premier pour choisir 
	printf("Rentrez un chiffre : \n"); //choix de l'option
	scanf("%d%*c",&choix);
	if (choix == 1)
		{printf("\n");affichageA(tNC,tN,tPR,tA,tCA,tPO,*nAdherent);printf("\n");}
	if (choix == 2)
		{printf("\n");insertpers(tNC,tN,tPR,tA,tCA,tPO,nAdherent);printf("\n");}
	if (choix == 3)
		{printf("\n");clientVenu(tNC,tCA,tPO,tA,idSports,nbPtSpo,venu);printf("\n");}
	if (choix == 4)
		{printf("\n");rajouteDpoints(tNC,tPO,nAdherent);printf("\n");}
	if (choix == 5)
		{printf("\n");supprpers(tNC,tN,tPR,tA,tCA,tPO,nAdherent);printf("\n");}
	if (choix == 9) //quitte le logiciel si l'utilisateur a rentré 9
		{printf("\n"); printf("Arrêt du logiciel"); printf("\n"); printf("\n");}
	if (choix != 9)
		{menuChoix (choix,idSports,nbPtSpo,nSports,tNC,tA,tCA,tPO,nAdherent,tN,tPR,venu,listeSports);return 0;}
}

int chargementadherent(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO) //j'ai renommé la fonction car c'est le plus le chargement que la création
{	
	int NC,N,PR,A,CA,PO, i=0;
	FILE *fe;
	fe=fopen("adherent.txt","r");
	if(fe == NULL)
	{
		printf("pbouvrfichier\n");
		return -1;
	}
	
	fscanf(fe,"%d %s %s %d %d %d",&NC,&N,&PR,&A,&CA,&PO);
	while (!feof(fe))
	{
		if(i==TAILLE)
		{
			printf("%d \t %d\n",i, TAILLE);
			printf("capacité atteinte\n");
			fclose(fe);
			return -1;
		}
		tNC[i]=NC;
		tN[i]=N;
		tPR[i]=PR;
		tA[i]=A;
		tCA[i]=CA;
		tPO[i]=PO;
		i++;
		fscanf(fe,"%d %s %s %d %d %d",&NC,&N,&PR,&A,&CA,&PO);

	}
	fclose(fe);
	return i;
}

int clientVenu (int *tNC, int *tCA, int *tPO, int *tA, int *idSports, int *nbPtSpo, char *venu)
{
	int arrive, position, positionSpo, activité, prix;
	char trouvoupas, creation, nvAct = 'O';
	printf("Quel client est arrivé ?\n");
	scanf("%d%*c",&arrive);
	position = rechercheSport(tNC,TAILLE,arrive,&trouvoupas);
	if (tCA[position] == 0)
	{
		printf("La carte du client est désactivée\n");
		return -1;
	}
	if (venu[position] == 'O')
	{
		printf("Erreur, le client est déjà venu aujourd'hui\n");
		return -1;
	}
	if (trouvoupas == 'N')
	{
		printf("Erreur, le client n'existe pas\n");
			return 0;
	}
	if (trouvoupas == 'O')
	{
		while (nvAct == 'O')
		{
			printf("Quelle activité va t-il faire ?\n");
			scanf("%d%*c",&activité);
			positionSpo = rechercheSport(idSports,TAILLE,activité,&trouvoupas);
			if (trouvoupas == 'N')
			{
				printf("Le sport n'existe pas\n");
				return -1;
			}
			prix = nbPtSpo[position];
			if (tA[position] <= 25) //réduction si le client à - de 25ans
				prix = prix * (85/100);
			if (tA[position] >= 65) //réduction si le client à + de 65ans
				prix = prix * (90/100);
			if (tPO[position] < prix)
			{
				printf("Erreur, le client n'as pas assez de points\n");
				return -1;
			}
			tPO[position] = prix;
			printf("Veut-il faire une autre activité ?\n");
			scanf("%c%*c",&nvAct);
		}
		venu[position] = 'O';		
	}
}

void affichageA(int *tNC, char *tN, char *tPR,int *tA, int *tCA,int *tPO, int n)
{
	int i ;
	printf("----------------------------------------------------------------------------\n");
	printf("n°client nom\t prenom\t age\t carte\t point\n");
	for (i=0;i<n;i++)
	{
		printf("%d\t %c\t %c\t %d\t %d\t %d\n",tNC[i],tN[i],tPR[i],tA[i],tCA[i],tPO[i]);
	}
	printf("----------------------------------------------------------------------------\n");
}

int rechercheclient(int *tNC,int n, int val, int* trouve)
{
	int i;
	for(i = 0; i < n; i++)
	{
		if (tNC[i]==val)
		{
			*trouve = 1;
			return i;
		}
		if(val<tNC[i])
		{
			*trouve = 0;
			return i;
		}

	}
	printf("le valeur %d n'est pas trouvé, sa position d'insertion est %d\n",val,i);
	*trouve=0;
	return i;
}

int insertpers(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO,int *nAdherent)
{
	int i, val, age, trouve, place;
	char nom, prenom;

	val = tNC[*nAdherent-1]+1;  
	i=rechercheclient(tNC,*nAdherent,val,&trouve);
	if(trouve == 0)
	{
		*nAdherent = *nAdherent+1;
		place = i;
		printf("%d",*nAdherent);
		if(*nAdherent ==TAILLE)
		{
			printf("table pleinne");
			return -1;
		}
		tNC[place]= val;
		printf("donnez la premiere lettre de votre nom");
		scanf("%c%*c",&nom);
		tN[place]= nom;
		printf("donnez la premiere lettre de votre prenom");
		scanf("%c%*c",&prenom);
		tPR[place] = prenom;
		printf("donnez votre age");
		scanf("%d%*c",&age);
		tA[place]= age;
		tCA[place]= 1;
		tPO[place]= 0;
	}
}
int supprpers(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO,int *nAdherent)
{
	int i, val, trouve=0;
	printf ("veillez indiquez le numéro de l'adhérent si vous ne voulez pas tappez -1\n");
	scanf("%d%*c",&val); 
	i=rechercheclient(tNC,*nAdherent,val,&trouve);
	if (val == -1)
	{
		return -1;
	}
	while (trouve != 1)
	{
		printf ("Veuillez indiquez le numéro de l'adhérent\n");
		scanf("%d%*c",&val);
		i=rechercheclient(tNC,*nAdherent,val,&trouve); 
		if (val == -1)
		{
			return -1;
		}
	}

	for (i; i < *nAdherent; i++) 
	{
		tNC[i] = tNC[i+1];
		tN[i] = tN[i+1];
		tPR[i] = tPR[i+1];
		tA[i] = tA[i+1];
		tCA[i] = tCA[i+1];
		tPO[i] = tPO[i+1];
	}
	*nAdherent = *nAdherent - 1; //réduit la taille logique
	return 1;

}



void sauvegardeaderents(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO,int nAdherent)
{
	int i;
	FILE *fe;
	fe = fopen("adherent.txt","w");
	for (i = 0; i < nAdherent; i++)
		fprintf(fe,"%d\t %d\t %d\t %d\t %d\t %d\n",tNC[i],tN[i],tPR[i],tA[i],tCA[i],tPO[i]);
	fclose(fe);
}

void rajouteDpoints(int *tNC,int *tPO,int *nAdherent)
{
	int point, numclient, i, trouve;
	float prix;
	char rep;

	printf("Numéro client ? -1 pour quitter");
	scanf("%d",&numclient);
	i=rechercheclient(tNC,*nAdherent,numclient,&trouve);
	while (trouve != 1)
	{

		printf ("erreur veillez indiquez le numéro de l'adhérent -1 pour quitter\n");
		scanf("%d%*c",&numclient);
		i=rechercheclient(tNC,*nAdherent,numclient,&trouve); 
		if (numclient == -1)
		{
			return;
		}
	}
	printf("Combien voulez-vous rajouter de points?\n");
	scanf("%d%*c",&point);
	prix = 0.5 * point;
	printf("le prix est de %.1f vouz confirmez? (O/N) \n",prix);
	scanf("%c%*c",&rep);
	if (rep == 'O')
	{
		tPO[i] = tPO[i] + point;
		printf("votre nombre de points est de %d \n",tPO[i]);
	}
		else
		{
			printf("action annulé retour au menu");
			return;
		}
}

void sauvegardeSports (int *idSports, int *pt, int n, char listeSports[][20])
{
	int i;
	FILE *fw;
	fw = fopen("sport.txt","w");
	for (i = 0; i < n; i++)
		fprintf(fw,"%d\t%d\t%s\n",idSports[i],pt[i],listeSports[i]);
	fclose(fw);
}

int chargementSport (int *idSports, int *nbPtSpo, int tmax, char listeSports[][20])
{
	int idSportTemp, ptTemp, i = 0;
	char sportact[20];
	FILE *fr;
	fr = fopen("sport.txt","r");
	if (fr == NULL) 
	{
		printf("Problème à l'ouverture du fichier");
		fclose(fr);
		return -1;
	}
	fscanf(fr,"%d%d%s",&idSportTemp,&ptTemp,sportact);
	while (!feof(fr))
	{
		if (i == tmax)
		{
			printf("Problème, tableau plein");
			return i;
		}
		idSports[i] = idSportTemp;
		nbPtSpo[i] = ptTemp;
		strcpy(listeSports[i],sportact);
		i++;
		fscanf(fr,"%d%d%s",&idSportTemp,&ptTemp,sportact);
	}
	fclose(fr);
	return i;
}

void affichage (int *idSports, int *pt, int n, char listeSports[][20])
{
	int i;
	printf("----------------------------------------------\n");
	printf("Sports:\t\tId:\tPoints:\n");
	for (i = 0; i < n; i++)
	{
		printf("%s:\t\t%d\t%d\n",listeSports[i],idSports[i],pt[i]);
	}
	printf("----------------------------------------------\n");
}


int rechercheSport(int *tablchoisi ,int tailletabl, int valacherch, char *trouvoupas)
{
	int i;
	for (i = 0; i<tailletabl; i++) 
	{
		if (tablchoisi[i] == valacherch) //il a trouvé
		{
			*trouvoupas = 'O';
			return i;
		}
		if (tablchoisi[i] > valacherch) //il est plus loin que la valeur a recherché donc a pas trouvé
		{
			*trouvoupas = 'N';
			return i;
		}	
	}
}

void ajoutSport (int *idSports, int *pt, int *n, int taillePhys,  char listeSports[][20])
{
	int nvSpo, nvNbPt, place, i;
	char trouvoupas , nvNom[20];
	if (*n == taillePhys) //vérifie qu'il y a encore de la place dans le tableau
	{
		printf("Erreur: la table est pleine");
		return;
	}
	printf("Numéro du nouveau sport: \n");
	scanf("%d%*c",&nvSpo);
	printf("Nombre de points du nouveau sport: \n");
	scanf("%d%*c",&nvNbPt);
	printf("Nom du nouveau sport");
	scanf("%s",nvNom);
	place = rechercheSport(idSports,*n,nvSpo,&trouvoupas); //donne où il doit ajouter le sport ou si il existe pas déjà
	if (trouvoupas == 'O') //si le sport existe déjà
	{
		printf("Erreur, le sport existe déjà\n");
		return;
	}
	for (i = *n - 1 ; i >= place; i--) //décale les sports après celui à ajouter
	{
		idSports[i+1] = idSports[i];
		pt[i+1] = pt[i];
		strcpy(listeSports[i+1],listeSports[i]);
	}
	idSports[place] = nvSpo; //ajoute le sport a sa place
	pt[place] = nvNbPt; //ajoute le nb de points a sa place
	strcpy(listeSports[place],nvNom);
	*n = *n + 1; //augmente la taille logique 
	return;
}

void changprix (int *idSport, int *pt, int n)
{
	int nvPrix , SpoChoix, place;
	char trouve;
	printf("Changer le prix de quel sport ?\n");
	scanf("%d%*c",&SpoChoix);
	place = rechercheSport(idSport,n,SpoChoix,&trouve);
	if (trouve == 'N')
	{
		printf("Erreur, le sport n'existe pas\n");
		return;
	}
	printf("Veuillez rentrer le nouveau prix: \n");
	scanf("%d%*c",&nvPrix);
	pt[place] = nvPrix ;
}

void enlèveSport (int *idSport, int *pt, int *n, char listeSports[][20])
{
	int place, i, supSpo;
	char trouvoupas;
	printf("Numéro du sport à supprimer: \n");
	scanf("%d%*c",&supSpo);
	place = rechercheSport(idSport,*n,supSpo,&trouvoupas); //donne la place du sport et si il l'a trouvé
	if (trouvoupas == 'N') //il ne l'a pas trouvé
	{
		printf("Erreur, le sport existe pas\n");
		return;
	}
	for (i = place ; i < *n - 1; i++) //décalle pour pouvoir supprimer le sport
	{
		idSport[i] = idSport[i+1];
		pt[i] = pt[i+1];
		strcpy(listeSports[i],listeSports[i+1]);
	}
	*n = *n - 1; //réduit la taille logique
	return;
}
