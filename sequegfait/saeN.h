/* \file: sae.h
   \author: Nolan Devouassoux, Renaud Beuret  
   \date: 25 oct
   
  */
 


 /* 
   \brief: gestion adhèrent
 */
int chargementadherent(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO);

 /* 
   \brief: affiche les adèrent
 */
void affichageA(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO, int n);
 
 /* 
   \brief: fonction qui recher le client 
 */
int rechercheclient(int*tNC,int n, int val, int* trouve); 
 /* 
   \brief: ajoute un adèrent
 */ 
int insertpers(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO,int *nAdherent);
 /* 
   \brief: suprime un adèrent
*/

int supprpers(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO,int *nAdherent);
 /* 
   \brief: sauvegarde le tableau dans le fichier adèrent
*/


void rajouteDpoints(int *tNC,int *tPO,int *nAdherent);
 /* 
   \brief: rajoute des points
*/

void sauvegardeaderents(int *tNC,char *tN,char *tPR,int *tA,int *tCA,int *tPO,int nAdherent);

 /* 
   \brief: globale
 */
void globale(); //pareil , mit en com et remplacé par mon tableau (où j'ai rajouté tes fonctions)

/*
   brief: charge les sports
*/
int chargementSport (int *idSports, int *nbPtSpo);

/*
   brief: affiche les sports
*/

void affichage (int *idSports, int *pt, int n);

/*
   brief: enlève un sport
*/

void enlèveSport (int *idSport, int *pt, int *n);

/*
   brief: menu
*/

void chargementInit (void);

/*
   brief: recherche sport 
*/

int rechercheSport (int *tablchoisi ,int tailletabl, int valacherch, char *trouvoupas);

void sauvegardeSports (int *idSports, int *pt, int n);

void ajoutSport (int *idSports, int *pt, int *n);

void changprix (int *idSport, int *pt, int n);

int clientVenu (int *tNC, int *tCA, int *tPO, int *idSports, int *nbPtSpo, char *venu);

int choixMenu (int choix, int *idSports, int *nbPtSpo, int *nSports, int *tNC, int *tA, int *tCA, int *tPO, int *nAdherent, char *tN, char *tPR, char *venu);